from verlet import *
from progress import progress
import matplotlib.pyplot as plt
from timeit import default_timer as timer

M = 6
rho = 0.55
T = 1.38
m = 48

t_end = 48
h = 0.032


model = Model(M, rho, T, m)
print(model)
temperatures = [model.temperature()]

start = timer()

t = [0]
progress(t[-1], t_end - t[0])
while t[-1] <= t_end:
	model.verlet_step(h)
	temperatures.append(model.temperature())

	t.append(t[-1] + h)
	progress(t[-1], t_end - t[0])


end = timer()
print(f"\nruntime: {round(end - start, 1)}s")

eq_time = 4
T_average = np.mean(temperatures[int(eq_time / 0.032):])
T_std = np.std(temperatures[int(eq_time / 0.032):])
print(f"T_average: {T_average} +- {T_std}")

plt.figure(dpi=200)
plt.plot(t, temperatures, color="lightseagreen", label=r"$T^*(t^*)$")
plt.axhline(T, color="orange", linestyle="--", label=r"$T_0^*$")
plt.axhline(T_average, color="violet", linestyle="--", label=r"$\langle T^* \rangle$")
plt.axvline(eq_time, color="black", alpha=0.7, linestyle="--", label="Equilibration time")
plt.xlabel(r"$t^*$")
plt.grid(alpha=0.6, linestyle="--")
plt.legend()
plt.savefig("plots/c.png")
plt.show()
