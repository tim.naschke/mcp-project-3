from verlet import *
from progress import progress
import matplotlib.pyplot as plt
from timeit import default_timer as timer

M = 6
rho = 0.55
T = 1.38
m = 48

t_end = 1
h = 0.032


model = Model(M, rho, T, m)
print(model)
melting_factors = [model.melting_factor()]

start = timer()

t = [0]
progress(t[-1], t_end - t[0])
while t[-1] <= t_end:
	model.verlet_step(h)
	melting_factors.append(model.melting_factor())

	t.append(t[-1] + h)
	progress(t[-1], t_end - t[0])


end = timer()
print(f"\nruntime: {round(end - start, 1)}s")

plt.figure(dpi=200)
plt.plot(t, melting_factors, label="$\\rho_k(t^*)$")
plt.axhline(3 * model.N, c="orange", alpha=0.8, linestyle="--", label=r"$3N$")
plt.xlabel(r"$t^*$")
plt.legend()
plt.savefig("plots/a_melt.png")
plt.show()

v = model.velocities
alpha = 0.6
plt.figure(dpi=200)
plt.hist(v[:,0], 50, alpha=alpha, color="r", label=r"$x$")
plt.hist(v[:,1], 50, alpha=alpha, color="g", label=r"$y$")
plt.hist(v[:,2], 50, alpha=alpha, color="b", label=r"$z$")
plt.legend()
plt.savefig("plots/a_velocity.png")
plt.show()
