from verlet import *
from progress import progress
import matplotlib.pyplot as plt
from timeit import default_timer as timer

M = 6
rho_arr = [1.2, 0.8, 0.3]
T_arr = [0.5, 1, 3]
m = 48

t_end = 4
eq_time = 0
h = 0.032


for i in range(len(rho_arr)):
	rho = rho_arr[i]
	T = T_arr[i]

	model = Model(M, rho, T, m)
	print(model)

	distances = []
	mean_squared_displacements = [model.mean_squared_displacement()]
	melting_factors = [model.melting_factor()]

	start = timer()

	t = [0]
	eq_steps = int(np.ceil(eq_time / h))
	progress(t[-1], t_end - t[0], "simulating     ")
	while t[-1] <= t_end:
		if i <= eq_steps and i % 20 == 0:
			model.temperature_rescaling()

		model.verlet_step(h, distances)
		mean_squared_displacements.append(model.mean_squared_displacement())
		melting_factors.append(model.melting_factor())

		t.append(t[-1] + h)
		progress(t[-1], t_end - t[0], "simulating")

	end = timer()

	print(f"\nruntime: {round(end - start, 1)}s")

	distances = np.array(distances).flatten()
	distances = distances[distances != 0]

	counts, bins = np.histogram(distances, range=(0, 2.5), bins=50)
	r = bins[:-1] + 0.025
	pair_correlation = counts / ((len(t) - 1) * model.N * (4 * np.pi * r**2 * 0.05) * rho)

	plt.figure(figsize=(10, 4))
	plt.suptitle(rf"$T_0^* = {T}$, $\rho^* = {rho}$")

	plt.subplot(1, 3, 1)
	plt.plot(r, pair_correlation)
	plt.title(r"$g(r^*)$")
	plt.xlabel(r"$r^*$")

	plt.subplot(1, 3, 2)
	plt.plot(t, mean_squared_displacements)
	plt.title(r"$\langle r*(t*)^2 \rangle$")
	plt.xlabel(r"$t^*$")

	plt.subplot(1, 3, 3)
	plt.plot(t, melting_factors)
	plt.title(r"$\rho_k(t*)$")
	plt.xlabel(r"$r^*$")

	plt.tight_layout()
	plt.savefig(f"plots/e_T{T}.png")
	plt.show()
