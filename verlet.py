import numpy as np


class Model:
	def __init__(self, M, rho, T, m):
		"""Initialize the positions and velocities of the system.

		:param M: number of unit cells along one axis of the cubic system
		:param rho: initial density of the system
		:param T: initial temperature of the system
		:param m: mass of the particles
		"""

		self.T = T
		self.N = 4 * M**3
		self.L = (4 / rho)**(1/3) * M
		self.a = self.L / M
		self.m = m

		rng = np.random.default_rng()

		self.positions = np.array([])
		self.velocities = rng.normal(0, np.sqrt(2/3 * T / 32), size=(self.N, 3))
		self.neighbors = np.array([])


		unit_cells = []

		for x in range(M):
			for y in range(M):
				for z in range(M):
					unit_cells.append(self.a * np.array([x, y, z]))

		relative_positions = [
			self.a * np.array([0, 0, 0]),
			self.a * np.array([1 / 2, 1 / 2, 0]),
			self.a * np.array([1 / 2, 0, 1 / 2]),
			self.a * np.array([0, 1 / 2, 1 / 2])
		]

		for cell in unit_cells:
			for rel_pos in relative_positions:
				x = cell + rel_pos
				x[x == 0] += 1e-12
				self.positions = np.append(self.positions, x)

		self.positions = self.positions.reshape(self.N, 3)
		self.initial_positions = self.positions.copy()

	def __str__(self):
		res = "# Simulation Parameters\n"
		res += f"N = {self.N}\n"
		res += f"T0 = {self.T}\n"
		res += f"L = {self.L}\n"
		res += f"a = {self.a}\n"
		res += f"m = {self.m}\n"

		return res

	def verlet_step(self, h, write_r=None):
		v_tilde = self.velocities + h / (2 * self.m) * self.force(write_r)
		self.positions += h * v_tilde
		self.positions = np.mod(self.positions, self.L)
		self.velocities = v_tilde + h / (2 * self.m) * self.force()

	def distances(self):
		distances = np.zeros((self.N, self.N, 3))

		for i in range(self.N):
			distances[i] = self.positions - self.positions[i]

		distances[distances > self.L / 2] -= self.L
		distances[distances < -self.L / 2] += self.L

		return distances

	def force(self, write_r=None):
		distances = self.distances()
		r = np.linalg.norm(distances, axis=2)

		if write_r is not None:
			write_r.append(r)

		np.fill_diagonal(r, 1)
		r = np.stack((r, r, r), axis=2)

		force = 48 * distances * (r**(-14) - r**(-8) / 2)

		force = np.sum(force, axis=0)
		return force

	def mean_squared_displacement(self):
		displacement = self.positions - self.initial_positions

		displacement[displacement > self.L / 2] -= self.L
		displacement[displacement < -self.L / 2] += self.L

		return np.mean(np.linalg.norm(displacement, axis=1)**2)

	def melting_factor(self):
		k = 4 * np.pi / self.a

		return np.sum(np.cos(k * self.positions))

	def temperature(self):
		return 16 * np.linalg.norm(self.velocities)**2 / self.N

	def temperature_rescaling(self):
		temp_actual = self.temperature()

		self.velocities *= (self.T / temp_actual)**0.5
