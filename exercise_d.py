from verlet import *
from progress import progress
import matplotlib.pyplot as plt
from timeit import default_timer as timer

M = 6
rho = 0.55
T = 1.38
m = 48

t_end = 48
eq_time = 4
h = 0.032


model = Model(M, rho, T, m)
print(model)

start = timer()

eq_steps = int(np.ceil(eq_time / h))
for i in range(eq_steps):
	progress(i, eq_steps, "equilibrating")

	if i % 20 == 0:
		model.temperature_rescaling()

	model.verlet_step(h)


temperatures = [model.temperature()]

t = [0]
progress(t[-1], t_end - t[0], "simulating     ")
while t[-1] <= t_end:
	model.verlet_step(h)
	temperatures.append(model.temperature())

	t.append(t[-1] + h)
	progress(t[-1], t_end - t[0], "simulating")


end = timer()
print(f"\nruntime: {round(end - start, 1)}s")

T_average = np.mean(temperatures)
print(f"T_average: {T_average} +- {np.std(temperatures)}")

plt.figure(dpi=200)
plt.plot(t, temperatures, color="lightseagreen", label=r"$T^*(t^*)$")
plt.axhline(T, color="orange", linestyle="--", label=r"$T_0^*$")
plt.axhline(T_average, color="violet", linestyle="--", label=r"$\langle T^* \rangle$")
plt.xlabel(r"$t^*$")
plt.grid(alpha=0.6, linestyle="--")
plt.legend()
plt.savefig("plots/d.png")
plt.show()
