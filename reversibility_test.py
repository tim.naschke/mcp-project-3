from verlet import *
from progress import progress

L = 100
M = 6
T = 1.38

steps = 10
h = 0.032


model = Model(L, M, T)
particles_start = model.particles.copy()

progress(0, 2 * steps, "simulating")
for i in range(steps):
	model.verlet_step(h)
	progress(i + 1, 2 * steps, "simulating")

progress(steps, 2 * steps, "reversing ")
model.reverse_velocity()

for i in range(steps):
	model.verlet_step(h)
	progress(steps + i + 1, 2 * steps, "reversing ")

particles_end = model.particles.copy()
particles_diff = np.array([])

for i in range(len(particles_start)):
	particles_diff = np.append(particles_diff, np.linalg.norm(particles_start[i].x - particles_end[i].x))

print("\n\nMax: ", np.max(particles_diff))
print(particles_diff)
